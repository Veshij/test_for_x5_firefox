import pytest
import allure

import time
from selenium import webdriver
from selenium.webdriver.common.by      import By
from selenium.webdriver.common.keys    import Keys
from selenium.webdriver.support.ui     import WebDriverWait
from selenium.webdriver.support        import expected_conditions as EC
from selenium.webdriver.chrome.options import Options

from selenium.webdriver import FirefoxOptions

from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

# Тестовые данные
password           = '111111X5'  					# Пароль от тестового пользователя
mail       	       = 'testerx52020@gmail.com'		# Почта тестового пользователя
gmail_mail         = 'https://www.gmail.com/mail/'  # Ресурс для тестирования
text_theme         = 'Hello'						# Тема письма


# Локаторы
button_mail_next     =  '//*[@id="identifierNext"]//div[2]'	   													# Кнопка далее на странице ввода почты
button_password_next =  '//*[@id="passwordNext"]//div[2]'														# Кнопка далее на странице ввода пароля
field_mail    		 =  '//*[@id="identifierId"]'																# Поле для ввода почты
field_pasword 	     =  '//*[@id="password"]//input'															# Поле для ввода пароля
write_letter		 =  '/html/body/div[7]/div[3]/div/div[2]/div[1]/div[1]/div[1]/div/div/div/div[1]/div/div'   # Кнопка "написать письмо"
whom 				 =  '//div/div/textarea'																	# Поле "Кому"
theme 			     =  '//form/div[3]/div/input'																# Тема письма
text_letter			 =  '//tbody/tr/td[2]/div[2]/div'														    # Текст письма
button_send_letter   =  '//table/tbody/tr[2]/td/div/div/div[4]/table/tbody/tr/td[1]/div/div[2]/div[1]'			# Кнопка "Отправить письмо"
sent_letters 		 =  '//div[4]/div/div/div[2]/span/a'														# Вкладка "Отправленные"
first_letter		 =  '//div[2]/div/table/tbody/tr[1]/td[5]'													# Локатор первого письма в разделе "Отправленные"
field_text_in_letter =  '//div[1]/div[2]/div[3]/div[3]/div/div[1]'												# Поле текста в письме
main_page 			 =  '//div[4]/div/a/img'																    # Переход на главную страницу


# Test Setup
with allure.step("Создаем вебдрайвер и случайную строку для определения корректности отправки письма."):
	#chrome_options = Options()
	#chrome_options.add_argument("start-maximized")
	#chrome_options.add_argument("--disable-infobars")
	#chrome_options.add_argument("--disable-extensions")
	#chrome_options.add_argument("--disable-gpu")
	#chrome_options.add_argument('--headless')
	#chrome_options.add_argument('--no-sandbox')
	#chrome_options.add_argument('--disable-dev-shm-usage')
	#chrome_options.add_argument('--window-size=1920,1080')
	#driver = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
	opts = FirefoxOptions()
	opts.add_argument("--headless")
	firefox_capabilities = DesiredCapabilities.FIREFOX
	firefox_capabilities['marionette'] = True
	firefox_capabilities['binary'] = '/usr/bin/firefox'
	driver = webdriver.Firefox(firefox_options=opts, capabilities=firefox_capabilities, executable_path="/builds/Veshij/test_for_x5_firefox/geckodriver")
	driver.maximize_window()
	#driver = webdriver.Firefox('/builds/Veshij/test_for_x5_firefox/geckodriver')
	time.sleep(5)
	#wait = WebDriverWait(driver, 10)

	
def wait_element(locator):	
	wait.until(EC.element_to_be_clickable(
    (By.XPATH, locator)))
	
with allure.step("Переходим на страницу почты Gmail."):
	driver.get("https://www.gmail.com/mail/")

with allure.step("Получаем объект поля для вводы почты и кнопки далее."):
	#wait_element(field_mail)
	time.sleep(5)
	wd_field_mail  =  driver.find_element(By.XPATH, field_mail)
	wd_mail_next   =  driver.find_element(By.XPATH, button_mail_next)

with allure.step("Вводим почту и нажимаем далее."):
	time.sleep(10)
	wd_field_mail.send_keys(mail)
	#wait_element(button_mail_next)
	time.sleep(10)
	wd_mail_next.click()

with allure.step("Получаем объект поля для ввода пароля и кнопки далее."):
	#wait_element(field_pasword)
	time.sleep(30)
	wd_field_password =  driver.find_element(By.XPATH, field_pasword)
	wd_password_next  =  driver.find_element(By.XPATH, button_password_next)

with allure.step("Вводим пароль и нажимаем далее."):
	wd_field_password.send_keys(password)
	#wait_element(button_password_next)
	time.sleep(15)
	wd_password_next.click()


@allure.feature('Проверка отправки сообщений.')
@allure.story('Тест проверки отправки сообщений через почтовый сервис gmail')
@pytest.mark.parametrize('mail', ['testerx52020@gmail.com', 'somemail@gmail.com'])
def test_gmail_mail(mail, random_text):
	with allure.step("Нажимаем написать письмо."):
		#wait_element(write_letter)
		time.sleep(15)
		wd_write_letter = driver.find_element(By.XPATH, write_letter)
		wd_write_letter.click()

	with allure.step("Получаем объекты вкладки отправленные, получателя, темы и поля текста и кнопки отправки письма."):
		#wait_element(whom)
		time.sleep(15)
		wd_whom 	    	  =  driver.find_element(By.XPATH, whom)
		wd_theme        	  =  driver.find_element(By.XPATH, theme)
		wd_text_letter 		  =  driver.find_element(By.XPATH, text_letter)
		wd_button_send_letter =  driver.find_element(By.XPATH, button_send_letter)

	with allure.step("Заполняем письмо и нажимаем кнопку отправить."):
		wd_whom.send_keys(mail)
		wd_theme.send_keys(text_theme)
		#wait_element(text_letter)
		time.sleep(15)
		wd_text_letter.send_keys(random_text)
		time.sleep(15)
		#wait_element(button_send_letter)
		wd_button_send_letter.click()

	with allure.step("Переходим во вкладку отправленные и выбираем первое письмо."):
		#wait_element(sent_letters)
		time.sleep(15)
		wd_sent_letters =  driver.find_element(By.XPATH, sent_letters)
		wd_sent_letters.click()
		time.sleep(15)
		#wait_element(first_letter)
		wd_first_letter = driver.find_element(By.XPATH, first_letter)
		wd_first_letter.click()

	with allure.step("Получаем текст письма и сравниваем его с заданным."):
		#wait_element(field_text_in_letter)
		time.sleep(15)
		wd_field_text_in_letter = driver.find_element(By.XPATH, field_text_in_letter)
		get_text = wd_field_text_in_letter.text
		assert random_text == get_text

	with allure.step("Переходим на главную страницу."):
		time.sleep(15)
		#wait_element(main_page)
		wd_main_page = driver.find_element(By.XPATH, main_page)
		wd_main_page.click()
